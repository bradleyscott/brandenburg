package api

import (
	"errors"
	"time"

	"github.com/globalsign/mgo"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/bradleyscott/brandenburg/pkg/types"
	"go.mongodb.org/mongo-driver/bson"
)

// MongoDBClient allows persistence to a db
type MongoDBClient struct {
	dbAccessRequests *mgo.Collection
	dbApplications   *mgo.Collection
}

// MongoDBClientConfig specifies settings required to connect to a db
type MongoDBClientConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	Database string
}

// NewMongoDBClient readies a MongoDBClient for use
func NewMongoDBClient(config *MongoDBClientConfig) (*MongoDBClient, error) {
	applyConfigDefaults(config)

	dbHost := config.Host + ":" + config.Port
	dialInfo := &mgo.DialInfo{
		Addrs:    []string{dbHost},
		Timeout:  60 * time.Second,
		Database: config.Database,
		Username: config.User,
		Password: config.Password,
	}

	session, err := mgo.DialWithInfo(dialInfo)
	if err != nil {
		log.WithFields(log.Fields{
			"dhHost":   dbHost,
			"database": config.Database,
			"user":     config.User,
			"password": config.Password,
			"err":      err.Error(),
		}).Error("A problem occurred connecting to the DB")
		return nil, err
	}
	session.SetMode(mgo.Monotonic, true)
	log.Debug("Successfully connected to DB")

	return &MongoDBClient{
		dbAccessRequests: session.DB(config.Database).C("accessRequests"),
		dbApplications:   session.DB(config.Database).C("applications"),
	}, nil
}

// RecordAccessChange adds an AccessChange record to an AccessRequest and sends notifications to targets
func (c *MongoDBClient) RecordAccessChange(change *types.AccessChange, accessRequestID string) (*types.AccessRequest, error) {

	if !change.IsValid() {
		message := "New AccessChange is not valid"
		log.WithField("accessChange", change).Debug(message)
		return nil, errors.New(message)
	}

	accessRequest, err := c.GetAccessRequest(accessRequestID)
	if err != nil {
		log.WithFields(log.Fields{
			"id":  accessRequestID,
			"err": err.Error(),
		}).Error("A problem occurred retrieving the AccessRequest record")
		return nil, err
	}

	accessRequest.AccessChanges = append(accessRequest.AccessChanges, change)
	err = c.dbAccessRequests.Update(bson.M{"id": accessRequestID}, &accessRequest)
	if err != nil {
		log.WithField("err", err.Error()).Error("A problem occurred writing the updated AccessRequest record")
		return nil, err
	}

	log.Info("AccessChange saved to DB")
	return accessRequest, nil
}

// GetAccessRequest returns the AccessRequest matching provided id
// Will return nil with no error if no matching AccessRequest exists
func (c *MongoDBClient) GetAccessRequest(id string) (*types.AccessRequest, error) {
	query := c.dbAccessRequests.Find(bson.M{"id": id})
	count, err := query.Count()
	if err != nil {
		log.WithFields(log.Fields{
			"id":  id,
			"err": err.Error(),
		}).Error("A problem occurred finding AccessRequest")
		return nil, err
	} else if count == 0 {
		log.WithField("id", id).Info("No matching AccessRequest found")
		return nil, nil
	}

	var accessRequest *types.AccessRequest
	err = query.One(&accessRequest)
	if err != nil {
		log.WithFields(log.Fields{
			"id":  id,
			"err": err.Error(),
		}).Error("A problem occurred finding AccessRequest")
		return nil, err
	}

	log.WithField("id", id).Info("Succesfully retrieved AccessRequest")
	return accessRequest, nil
}

// CreateAccessRequest returns an existing matching or newly created AccessRequest and sends notifications to targets
func (c *MongoDBClient) CreateAccessRequest(proposed *types.AccessRequest) (*types.AccessRequest, error) {

	if !proposed.IsValid() {
		message := "AccessRequest is not valid"
		log.WithField("accessRequest", proposed).Debug(message)
		return nil, errors.New(message)
	}

	// Search for and return existing AccessRequest
	query := c.dbAccessRequests.Find(bson.M{
		"requestor":      proposed.Requestor,
		"application.id": proposed.Application.ID,
	})
	count, err := query.Count()
	if err != nil {
		log.WithField("err", err.Error()).Error("A problem occurred trying to find an existing matching AccessRequest")
		return nil, err
	} else if count != 0 {
		var match *types.AccessRequest
		err := query.One(&match)

		if err != nil {
			log.WithField("err", err.Error()).Error("A problem occurred trying to marshal the existing matching AccessRequest")
			return nil, err
		}

		log.Info("Found existing matching AccessRequest")
		return match, nil
	}

	// Save the access request
	id, _ := uuid.NewUUID()
	proposed.ID = id.String()
	err = c.dbAccessRequests.Insert(&proposed)
	if err != nil {
		log.WithField("err", err.Error()).Error("A problem occurred writing the AccessRequest record")
		return nil, err
	}

	log.WithField("id", proposed.ID).Info("AccessRequest saved to DB")
	return proposed, nil
}

// GetApplications returns all Application records
func (c *MongoDBClient) GetApplications() ([]*types.Application, error) {
	var applications []*types.Application
	err := c.dbApplications.Find(nil).All(&applications)
	if err != nil {
		log.WithField("err", err.Error()).Error("A problem occurred retrieving Applications")
		return nil, err
	}

	log.WithField("count", len(applications)).Info("Successfully retrieved Applications")
	return applications, nil
}

// GetApplication returns a matching Application
// If none is found, a nil Application and nil error is returned
func (c *MongoDBClient) GetApplication(id string) (*types.Application, error) {

	query := c.dbApplications.Find(bson.M{"id": id})
	count, err := query.Count()
	if err != nil {
		log.WithFields(log.Fields{
			"id":  id,
			"err": err.Error(),
		}).Error("A problem occurred finding Application")
		return nil, err
	} else if count == 0 {
		log.WithField("id", id).Info("No matching Application found")
		return nil, nil
	}

	var application *types.Application
	err = query.One(&application)
	if err != nil {
		log.WithFields(log.Fields{
			"id":  id,
			"err": err.Error(),
		}).Error("A problem occurred marshalling matching Application")
		return nil, err
	}

	log.WithField("id", id).Info("Succesfully retrieved Application")
	return application, nil
}

// CreateApplication returns an existing matching or newly created Application
func (c *MongoDBClient) CreateApplication(proposed *types.Application) (*types.Application, error) {

	if !proposed.IsValid() {
		message := "Application is not valid"
		log.WithField("application", proposed).Debug(message)
		return nil, errors.New(message)
	}

	// Search for and return existing AccessRequest
	query := c.dbApplications.Find(bson.M{"name": proposed.Name})
	count, err := query.Count()
	if err != nil {
		log.WithField("err", err.Error()).Error("A problem occurred trying to find an existing matching Application")
		return nil, err
	} else if count != 0 {
		var match *types.Application
		err := query.One(&match)
		if err != nil {
			log.WithField("err", err.Error()).Error("A problem occurred marshalling existing matching Application")
			return nil, err
		}

		log.WithFields(log.Fields{
			"name": proposed.Name,
			"id":   match.ID,
		}).Info("Found existing matching Application")
		return match, nil
	}

	// Save the access request
	id, _ := uuid.NewUUID()
	proposed.ID = id.String()
	err = c.dbApplications.Insert(&proposed)
	if err != nil {
		log.WithField("err", err.Error()).Error("A problem occurred writing the Application record")
		return nil, err
	}

	log.WithField("id", proposed.ID).Info("Application saved to DB")
	return proposed, nil
}

// RecordApproval adds an Approval record to an AccessRequest and sends notifications to targets
func (c *MongoDBClient) RecordApproval(approval *types.Approval, accessRequestID string) (*types.AccessRequest, error) {

	if !approval.IsValid() {
		message := "New Approval is not valid"
		log.WithField("approval", approval).Debug(message)
		return nil, errors.New(message)
	}

	accessRequest, err := c.GetAccessRequest(accessRequestID)
	if err != nil {
		message := "A problem occurred retrieving the AccessRequest record"
		log.WithField("accessRequestID", accessRequestID).Error(message)
		return nil, err
	}

	accessRequest.Approvals = append(accessRequest.Approvals, approval)
	err = c.dbAccessRequests.Update(bson.M{"id": accessRequestID}, &accessRequest)
	if err != nil {
		log.WithField("err", err.Error()).Error("A problem occurred writing the updated AccessRequest record")
		return nil, err
	}

	log.Info("Approval saved to db")
	return accessRequest, nil
}

func applyConfigDefaults(config *MongoDBClientConfig) {
	if config.Host == "" {
		config.Host = "db"
	}
	if config.Port == "" {
		config.Port = "27017"
	}
	if config.Database == "" {
		config.Database = "brandenburg"
	}
}
