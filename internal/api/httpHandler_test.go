package api

import (
	"bytes"
	"errors"
	"net/http/httptest"
	"testing"

	"gitlab.com/bradleyscott/brandenburg/pkg/types"
)

type mockBrandenburgClient struct {
}

func (client mockBrandenburgClient) GetAccessRequest(id string) (*types.AccessRequest, error) {
	return nil, errors.New("not implemented")
}
func (client mockBrandenburgClient) CreateAccessRequest(proposed *types.AccessRequest) (*types.AccessRequest, error) {
	return nil, errors.New("not implemented")
}
func (client mockBrandenburgClient) RecordAccessChange(change *types.AccessChange, accessRequestID string) (*types.AccessRequest, error) {
	return nil, errors.New("not implemented")
}
func (client mockBrandenburgClient) RecordApproval(approval *types.Approval, accessRequestID string) (*types.AccessRequest, error) {
	return nil, errors.New("not implemented")
}
func (client mockBrandenburgClient) GetApplications() ([]*types.Application, error) {
	return nil, errors.New("not implemented")
}
func (client mockBrandenburgClient) GetApplication(id string) (*types.Application, error) {
	return nil, errors.New("not implemented")
}
func (client mockBrandenburgClient) CreateApplication(proposed *types.Application) (*types.Application, error) {
	return nil, errors.New("not implemented")
}

type mockNotifier struct {
}

func (notifier mockNotifier) Notify(notification *types.Notification) error {
	return errors.New("not implemented")
}

func TestConstruction(t *testing.T) {
	handler := NewHTTPHandler(mockBrandenburgClient{}, mockNotifier{})

	if handler.client == nil || handler.notifier == nil {
		t.Error("handler construction failed")
	}
}

func TestGetPing(t *testing.T) {
	handler := NewHTTPHandler(mockBrandenburgClient{}, mockNotifier{})

	w := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "http://example.com/foo", nil)
	handler.GetPing(w, req)

	expected := []byte("Pong")
	actual := w.Body.Bytes()
	if !bytes.Equal(actual, expected) {
		t.Errorf("Ping test failed. Expected: %s. Got: %s", expected, actual)
	}

}
