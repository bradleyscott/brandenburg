package api

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bradleyscott/brandenburg/pkg/types"
)

// HTTPNotifier sends HTTP POSTs to targets including Notification payloads
type HTTPNotifier struct {
	Targets []url.URL
}

// NewHTTPNotifier returns a completely initialised HTTPNotifier
func NewHTTPNotifier(targets []string) (*HTTPNotifier, error) {
	var notifier HTTPNotifier
	urlTargets := make([]url.URL, len(targets))
	for i, target := range targets {
		u, err := url.Parse(target)
		if err != nil {
			log.WithFields(log.Fields{
				"target": target,
				"err":    err.Error(),
			}).Error("Unable to parse target to url")
			return nil, err
		}
		urlTargets[i] = *u
	}
	notifier.Targets = urlTargets
	return &notifier, nil
}

func (h *HTTPNotifier) post(target url.URL, payload interface{}) error {
	body, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	var resp *http.Response
	resp, err = http.Post(target.String(), "application/json", bytes.NewBuffer(body))
	if err != nil {
		message := "A problem occurred when sending notification to target"
		log.WithFields(log.Fields{
			"target": target.String(),
			"err":    err.Error(),
		}).Warn(message)
		return err
	}

	defer resp.Body.Close()
	if resp.StatusCode >= 300 {
		message := "A problem occurred when sending notification to target"
		log.WithFields(log.Fields{
			"target":     target.String(),
			"statusCode": resp.StatusCode,
		}).Warn(message)
		return errors.New(message)
	}

	return nil
}

// Notify will POST a Notification to all targets
func (h *HTTPNotifier) Notify(notification *types.Notification) error {

	for _, target := range h.Targets {
		err := h.post(target, notification)
		if err == nil {
			log.WithField("target", target.String()).Info("Notification sent")
		}
	}
	return nil
}
