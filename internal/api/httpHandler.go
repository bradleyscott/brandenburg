package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bradleyscott/brandenburg/pkg/types"
)

// HTTPHandler can handle incoming HTTP requests in fiber
type HTTPHandler struct {
	client   types.BrandenburgClient
	notifier types.Notifier
}

// NewHTTPHandler initialises a new HTTPHandler with the provided client
func NewHTTPHandler(client types.BrandenburgClient, notifier types.Notifier) HTTPHandler {
	handler := HTTPHandler{
		client:   client,
		notifier: notifier,
	}
	return handler
}

// PostAccessChange records an AccessChange to an AccessRequest using POST body and query string params
func (handler HTTPHandler) PostAccessChange(w http.ResponseWriter, r *http.Request) {

	id := mux.Vars(r)["accessRequestID"]
	if id == "" {
		message := "A valid AccessRequestID is required in POST body or query string"
		log.WithFields(log.Fields{
			"queryParam": id,
		}).Info(message)
		handler.sendError(400, message, w)
		return
	}

	var a *types.AccessChange
	err := json.NewDecoder(r.Body).Decode(&a)
	if err != nil {
		log.WithField("error", err.Error()).Info("Unable to parse request body")
		handler.sendError(400, err.Error(), w)
		return
	} else if a.IsValid() == false {
		message := "Request body does not contain a valid AccessChange"
		body, _ := ioutil.ReadAll(r.Body)
		log.WithField("body", body).Info(message)
		handler.sendError(400, message, w)
		return
	}

	updatedRequest, err := handler.client.RecordAccessChange(a, id)
	if err != nil {
		handler.sendError(500, err.Error(), w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(updatedRequest)

	// Send notification
	notification := &types.Notification{
		Type:          types.NotificationTypes.AccessChanged,
		AccessRequest: updatedRequest,
		AccessChange:  a,
	}
	go handler.notifier.Notify(notification)
}

// GetAccessRequest returns 200 and a matching AccessRequest
func (handler HTTPHandler) GetAccessRequest(w http.ResponseWriter, r *http.Request) {

	id := mux.Vars(r)["id"]
	if id == "" {
		message := "A valid AccessRequestID is required"
		log.WithField("queryParam", id).Info(message)
		handler.sendError(400, message, w)
		return
	}

	accessRequest, err := handler.client.GetAccessRequest(id)
	if err != nil {
		handler.sendError(500, err.Error(), w)
		return
	} else if accessRequest == nil {
		handler.sendError(404, "No matching AccessRequest found", w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(accessRequest)
}

// PostAccessRequest creates a new AccessRequest from POST body
func (handler HTTPHandler) PostAccessRequest(w http.ResponseWriter, r *http.Request) {

	var a *types.AccessRequest
	err := json.NewDecoder(r.Body).Decode(&a)

	if err != nil {
		log.WithField("error", err.Error()).Info("Unable to parse request body")
		handler.sendError(400, err.Error(), w)
		return
	} else if !a.IsValid() {
		message := "Request body does not contain a valid AccessRequest"
		body, _ := ioutil.ReadAll(r.Body)
		log.WithField("body", string(body)).Info(message)
		handler.sendError(400, message, w)
		return
	}

	newRequest, err := handler.client.CreateAccessRequest(a)
	if err != nil {
		handler.sendError(500, err.Error(), w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newRequest)

	// Send notification
	notification := &types.Notification{
		Type:          types.NotificationTypes.RequestCreated,
		AccessRequest: newRequest,
	}
	go handler.notifier.Notify(notification)
}

// GetApplications return 200 containing a JSON array of Applications
func (handler HTTPHandler) GetApplications(w http.ResponseWriter, r *http.Request) {
	applications, err := handler.client.GetApplications()
	if err != nil {
		handler.sendError(500, err.Error(), w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(applications)
}

// GetApplication returns 200 and a matching Application
func (handler HTTPHandler) GetApplication(w http.ResponseWriter, r *http.Request) {

	id := mux.Vars(r)["id"]
	if id == "" {
		message := "A valid ApplicationID is required"
		log.WithField("queryParam", id).Info(message)
		handler.sendError(400, message, w)
		return
	}

	application, err := handler.client.GetApplication(id)
	if err != nil {
		handler.sendError(500, err.Error(), w)
		return
	} else if application == nil {
		handler.sendError(404, "No matching Application found", w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(application)
}

// PostApproval records an Approval to an AccessRequest using POST body and query string params
func (handler HTTPHandler) PostApproval(w http.ResponseWriter, r *http.Request) {

	id := mux.Vars(r)["accessRequestID"]
	if id == "" {
		message := "A valid AccessRequestID is required in query string"
		log.WithField("queryParam", id).Info(message)
		handler.sendError(400, message, w)
		return
	}

	var a *types.Approval
	err := json.NewDecoder(r.Body).Decode(&a)

	if err != nil {
		log.WithField("error", err.Error()).Info("Unable to parse request body")
		handler.sendError(400, err.Error(), w)
		return
	} else if a.IsValid() == false {
		message := "Request body does not contain a valid Approval"
		body, _ := ioutil.ReadAll(r.Body)
		log.WithField("body", body).Info(message)
		handler.sendError(400, message, w)
		return
	}

	updatedRequest, err := handler.client.RecordApproval(a, id)
	if err != nil {
		handler.sendError(500, err.Error(), w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(updatedRequest)

	// Send notification
	notification := &types.Notification{
		Type:          types.NotificationTypes.ApprovalReceived,
		AccessRequest: updatedRequest,
		Approval:      a,
	}
	go handler.notifier.Notify(notification)
}

// PostApplication creates a new AccessRequest from POST body
func (handler HTTPHandler) PostApplication(w http.ResponseWriter, r *http.Request) {

	var a *types.Application
	err := json.NewDecoder(r.Body).Decode(&a)

	if err != nil {
		log.WithField("error", err.Error()).Info("Unable to parse request body")
		handler.sendError(400, err.Error(), w)
		return
	} else if !a.IsValid() {
		message := "Request body does not contain a valid Application"
		body, _ := ioutil.ReadAll(r.Body)
		log.WithField("body", string(body)).Info(message)
		handler.sendError(400, message, w)
		return
	}

	newRequest, err := handler.client.CreateApplication(a)
	if err != nil {
		handler.sendError(500, err.Error(), w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newRequest)
}

// GetPing will return a 200 Pong in all cases
func (handler HTTPHandler) GetPing(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Pong"))
}

func (handler HTTPHandler) sendError(status int, message string, w http.ResponseWriter) {
	type ErrorResponse struct {
		StatusCode   int    `json:"status"`
		ErrorMessage string `json:"message"`
	}

	response := ErrorResponse{
		StatusCode:   status,
		ErrorMessage: message,
	}
	payload, _ := json.Marshal(&response)
	w.WriteHeader(response.StatusCode)
	w.Write(payload)
}
