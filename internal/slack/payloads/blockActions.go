package payloads

// BlockActionsPayload describes the payload received from Slack when a user clicks an interactive button in a message
type BlockActionsPayload struct {
	Type string `json:"type"`
	User struct {
		ID       string `json:"id"`
		Username string `json:"username"`
		Name     string `json:"name"`
		TeamID   string `json:"team_id"`
	} `json:"user"`
	APIAppID  string `json:"api_app_id"`
	Token     string `json:"token"`
	Container struct {
		Type        string `json:"type"`
		MessageTs   string `json:"message_ts"`
		ChannelID   string `json:"channel_id"`
		IsEphemeral bool   `json:"is_ephemeral"`
	} `json:"container"`
	TriggerID string `json:"trigger_id"`
	Team      struct {
		ID     string `json:"id"`
		Domain string `json:"domain"`
	} `json:"team"`
	Channel struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	} `json:"channel"`
	Message struct {
		BotID  string `json:"bot_id"`
		Type   string `json:"type"`
		Text   string `json:"text"`
		User   string `json:"user"`
		Ts     string `json:"ts"`
		Team   string `json:"team"`
		Blocks []struct {
			Type    string `json:"type"`
			BlockID string `json:"block_id"`
			Text    struct {
				Type     string `json:"type"`
				Text     string `json:"text"`
				Verbatim bool   `json:"verbatim"`
			} `json:"text,omitempty"`
		} `json:"blocks"`
	} `json:"message"`
	ResponseURL string `json:"response_url"`
	Actions     []struct {
		ActionID string `json:"action_id"`
		BlockID  string `json:"block_id"`
		Text     struct {
			Type  string `json:"type"`
			Text  string `json:"text"`
			Emoji bool   `json:"emoji"`
		} `json:"text"`
		Value    string `json:"value"`
		Style    string `json:"style"`
		Type     string `json:"type"`
		ActionTs string `json:"action_ts"`
	} `json:"actions"`
}
