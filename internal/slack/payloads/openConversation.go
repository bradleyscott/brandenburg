package payloads

// OpenConversationResponse is returned by Slack after a conversation.open request
type OpenConversationResponse struct {
	Ok      bool `json:"ok"`
	Channel struct {
		ID string `json:"id"`
	} `json:"channel"`
}

// OpenConversationRequest is the payload sent to Slack during the conversation.open request
type OpenConversationRequest struct {
	Users string `json:"users"`
}
