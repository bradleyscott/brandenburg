package payloads

// SlackResponse is returned by Slack in response to all calls to its API
type SlackResponse struct {
	Ok bool `json:"ok"`
}
