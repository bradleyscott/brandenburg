package payloads

// ChatUpdateRequest is the payload sent to Slack during the chat.update request
type ChatUpdateRequest struct {
	Text      string `json:"text"`
	Channel   string `json:"channel"`
	Timestamp string `json:"ts"`
	Blocks    string `json:"blocks"`
}

// ChatUpdateResponse is returned by Slack after a chat.update request
type ChatUpdateResponse struct {
	Ok      bool   `json:"ok"`
	Channel string `json:"channel"`
	Ts      string `json:"ts"`
	Message struct {
		Text        string `json:"text"`
		Username    string `json:"username"`
		BotID       string `json:"bot_id"`
		Attachments []struct {
			Text     string `json:"text"`
			ID       int    `json:"id"`
			Fallback string `json:"fallback"`
		} `json:"attachments"`
		Type    string `json:"type"`
		Subtype string `json:"subtype"`
		Ts      string `json:"ts"`
	} `json:"message"`
}
