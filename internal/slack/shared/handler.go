package shared

import "gitlab.com/bradleyscott/brandenburg/pkg/types"

// EventHandler can process incoming Slack event payloads
type EventHandler interface {
	ProcessPayload(payload string) (string, error)
}

// NotificationHandler can process incoming brandenburg Notifications
type NotificationHandler interface {
	ProcessNotification(notification *types.Notification) (string, error)
}

// HandlerOptions contains relevant dependencies for initialising Handlers
type HandlerOptions struct {
	Renderer    TemplateRenderer
	APIClient   types.BrandenburgClient
	SlackClient Slacker
}
