package shared

import (
	"errors"

	"github.com/cbroglie/mustache"
	log "github.com/sirupsen/logrus"
)

// TemplateRenderer describes the ability to render text templates
type TemplateRenderer interface {
	RenderTemplate(template string, context interface{}) (string, error)
}

// MustacheRenderer uses Mustache json templates stored on disk
type MustacheRenderer struct{}

// RenderTemplate renders a mustache template in the templates/ folder using the provided context
func (m MustacheRenderer) RenderTemplate(template string, context interface{}) (string, error) {
	filename := "templates/" + template + ".json"
	output, err := mustache.RenderFile(filename, context)

	if err != nil {
		message := "There was a problem trying to render mustache template"
		log.WithFields(log.Fields{
			"template": template,
			"err":      err.Error(),
		}).Warn(message)
		return "", errors.New(message)
	}

	return output, nil
}
