package routers

import (
	"encoding/json"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bradleyscott/brandenburg/internal/slack/shared"
)

// EventRouter routes any event payloads coming from Slack
type EventRouter struct {
	ShortcutHandler       shared.EventHandler
	ViewSubmissionHandler shared.EventHandler
	BlockActionsHandler   shared.EventHandler
}

// Process routes incoming POST payloads from Slack
func (e *EventRouter) Process(w http.ResponseWriter, r *http.Request) {

	err := r.ParseForm()
	body := r.Form.Get("payload") // Slack payloads have json inside payload form field

	// Parse incoming payload
	type payload struct{ Type string }
	var incomingPayload payload
	err = json.Unmarshal([]byte(body), &incomingPayload)

	if err != nil {
		log.WithField("payload", body).Warn("Unable to parse POST body")
		e.sendError(400, err.Error(), w)
		return
	}

	// Route to the right handler depending on payload Type
	var responseJSON string
	switch incomingPayload.Type {
	case "shortcut":
		responseJSON, err = e.ShortcutHandler.ProcessPayload(string(body))
	case "view_submission":
		responseJSON, err = e.ViewSubmissionHandler.ProcessPayload(string(body))
	case "block_actions":
		responseJSON, err = e.BlockActionsHandler.ProcessPayload(string(body))
	default:
		message := "No handler for this payload type"
		log.WithField("payloadType", incomingPayload.Type).Warn(message)
		e.sendError(400, message, w)
		return
	}

	// Handle any errors from handler
	if err != nil {
		e.sendError(500, err.Error(), w)
		return
	}
	// Send JSON reply
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(responseJSON))
}

func (e *EventRouter) sendError(status int, message string, w http.ResponseWriter) {
	type ErrorResponse struct {
		StatusCode   int    `json:"status"`
		ErrorMessage string `json:"message"`
	}

	response := ErrorResponse{
		StatusCode:   status,
		ErrorMessage: message,
	}
	payload, _ := json.Marshal(&response)
	w.WriteHeader(response.StatusCode)
	w.Write(payload)
}
