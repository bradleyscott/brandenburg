package notificationhandlers

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bradleyscott/brandenburg/internal/slack/shared"
	"gitlab.com/bradleyscott/brandenburg/pkg/types"
)

// RequestCreatedNotificationHandler can handle RequestCreated notifications
type RequestCreatedNotificationHandler struct {
	Renderer    shared.TemplateRenderer
	APIClient   types.BrandenburgClient
	SlackClient shared.Slacker
}

// NewRequestCreatedNotificationHandler initialises a new handler based on provided options
func NewRequestCreatedNotificationHandler(options shared.HandlerOptions) RequestCreatedNotificationHandler {
	handler := RequestCreatedNotificationHandler{
		Renderer:    options.Renderer,
		APIClient:   options.APIClient,
		SlackClient: options.SlackClient,
	}
	return handler
}

// ProcessNotification confirms the AccessRequest has been submitted, and will request Approvals or access changes if required
func (h RequestCreatedNotificationHandler) ProcessNotification(notification *types.Notification) (string, error) {
	log.Info("RequestedCreated notification received")

	if notification.AccessRequest.IsApprovalRequired() {
		go h.requestApproval(notification.AccessRequest)
	} else {
		go h.requestAccessChange(notification.AccessRequest)
	}

	return "", nil
}

func (h RequestCreatedNotificationHandler) requestAccessChange(accessRequest *types.AccessRequest) error {
	recipients := accessRequest.Application.Owners
	channel, err := h.SlackClient.OpenConversation(recipients)
	if err != nil {
		return err
	}

	var blocks string
	blocks, err = h.renderRequestAccessChangeTemplate(accessRequest)
	if err != nil {
		return err
	}

	message := fmt.Sprintf("An access request for %s is awaiting provisioning", accessRequest.Application.Name)
	return h.SlackClient.PostMessage(channel, blocks, message)
}

func (h RequestCreatedNotificationHandler) requestApproval(accessRequest *types.AccessRequest) error {
	recipients := accessRequest.Application.Approvers
	channel, err := h.SlackClient.OpenConversation(recipients)
	if err != nil {
		return err
	}

	var blocks string
	blocks, err = h.renderRequestApprovalTemplate(accessRequest)
	if err != nil {
		return err
	}

	message := fmt.Sprintf("An access request for %s is awaiting your approval", accessRequest.Application.Name)
	return h.SlackClient.PostMessage(channel, blocks, message)
}

func (h RequestCreatedNotificationHandler) renderRequestApprovalTemplate(accessRequest *types.AccessRequest) (string, error) {
	type templateContext struct {
		Requestor     string
		AccessRequest *types.AccessRequest
	}

	requestor, _ := h.SlackClient.GetUsername(accessRequest.Requestor)
	context := templateContext{
		Requestor:     requestor,
		AccessRequest: accessRequest,
	}

	return h.Renderer.RenderTemplate("requestApproval", context)
}

func (h RequestCreatedNotificationHandler) renderRequestAccessChangeTemplate(accessRequest *types.AccessRequest) (string, error) {
	type templateContext struct {
		Requestor     string
		AccessRequest *types.AccessRequest
	}

	requestor, _ := h.SlackClient.GetUsername(accessRequest.Requestor)
	context := templateContext{
		Requestor:     requestor,
		AccessRequest: accessRequest,
	}

	return h.Renderer.RenderTemplate("requestAccessChange", context)
}
