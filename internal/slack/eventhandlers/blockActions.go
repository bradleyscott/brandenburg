package eventhandlers

import (
	"encoding/json"
	"errors"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bradleyscott/brandenburg/internal/slack/payloads"
	"gitlab.com/bradleyscott/brandenburg/internal/slack/shared"
	"gitlab.com/bradleyscott/brandenburg/pkg/types"
)

// BlockActionsEventHandler can handle view_submission paylods from Slack
type BlockActionsEventHandler struct {
	Renderer    shared.TemplateRenderer
	APIClient   types.BrandenburgClient
	SlackClient shared.Slacker
}

// NewBlockActionsEventHandler initialises a new handler based on provided options
func NewBlockActionsEventHandler(options shared.HandlerOptions) *BlockActionsEventHandler {
	handler := &BlockActionsEventHandler{
		Renderer:    options.Renderer,
		APIClient:   options.APIClient,
		SlackClient: options.SlackClient,
	}
	return handler
}

// ProcessPayload processes view_submission payloads
func (h *BlockActionsEventHandler) ProcessPayload(payload string) (string, error) {

	var incomingPayload payloads.BlockActionsPayload
	err := json.Unmarshal([]byte(payload), &incomingPayload)
	if err != nil {
		log.WithField("err", err.Error()).Error("Unable to parse block_actions payload")
		return "", err
	}

	switch incomingPayload.Actions[0].BlockID {
	case "approval":
		log.Debug("Approval event received")
		approval, err := h.recordApproval(incomingPayload)
		if err != nil {
			return "", err
		}

		err = h.updateApprovalRequestMessage(incomingPayload, approval)
		if err != nil {
			return "", err
		}
	case "accessChange":
		log.Debug("AccessChange event received")
		accessChange, err := h.recordAccessChange(incomingPayload)
		if err != nil {
			return "", err
		}

		err = h.updateAccessChangeRequestMessage(incomingPayload, accessChange)
		if err != nil {
			return "", err
		}
	default:
		message := "No handler exists for this block_actions type"
		log.WithField("blockID", incomingPayload.Actions[0].BlockID).Warn(message)
		return "", errors.New(message)
	}

	return "", nil
}

func (h *BlockActionsEventHandler) updateApprovalRequestMessage(incomingPayload payloads.BlockActionsPayload, approval *types.Approval) error {
	requestID := strings.Split(incomingPayload.Actions[0].Value, ":")[1]
	accessRequest, err := h.APIClient.GetAccessRequest(requestID)
	if err != nil {
		return err
	}

	var update string
	update, err = h.renderEditApprovalRequestTemplate(accessRequest, approval)
	if err != nil {
		return err
	}

	return h.SlackClient.ChatUpdate(incomingPayload.Channel.ID, incomingPayload.Message.Ts, update, "Access approval request actioned")
}

func (h *BlockActionsEventHandler) updateAccessChangeRequestMessage(incomingPayload payloads.BlockActionsPayload, accessChange *types.AccessChange) error {
	requestID := strings.Split(incomingPayload.Actions[0].Value, ":")[1]
	accessRequest, err := h.APIClient.GetAccessRequest(requestID)
	if err != nil {
		return err
	}

	var update string
	update, err = h.renderEditAccessChangeRequestTemplate(accessRequest, accessChange)
	if err != nil {
		return err
	}

	return h.SlackClient.ChatUpdate(incomingPayload.Channel.ID, incomingPayload.Message.Ts, update, "Access change request actioned")
}

func (h *BlockActionsEventHandler) recordAccessChange(incomingPayload payloads.BlockActionsPayload) (*types.AccessChange, error) {

	owner, err := h.SlackClient.GetEmail(incomingPayload.User.ID)
	if err != nil {
		log.WithFields(log.Fields{
			"owner": incomingPayload.User.ID,
			"err":   err.Error(),
		}).Error("A problem occurred when getting owner email")
		return nil, err
	}

	var accessChangeType string
	switch strings.Split(incomingPayload.Actions[0].Value, ":")[0] {
	case "completed":
		accessChangeType = types.AccessChangeTypes.Granted
	case "decline":
		accessChangeType = types.AccessChangeTypes.Declined
	}

	accessChange := &types.AccessChange{
		Owner:     owner,
		Type:      accessChangeType,
		ChangedAt: time.Now(),
	}

	requestID := strings.Split(incomingPayload.Actions[0].Value, ":")[1]
	_, err = h.APIClient.RecordAccessChange(accessChange, requestID)
	if err != nil {
		return nil, err
	}

	return accessChange, nil
}

func (h *BlockActionsEventHandler) recordApproval(incomingPayload payloads.BlockActionsPayload) (*types.Approval, error) {

	approver, err := h.SlackClient.GetEmail(incomingPayload.User.ID)
	if err != nil {
		log.WithFields(log.Fields{
			"approver": incomingPayload.User.ID,
			"err":      err.Error(),
		}).Error("A problem occurred when getting approver email")
		return nil, err
	}

	var approvalType string
	switch strings.Split(incomingPayload.Actions[0].Value, ":")[0] {
	case "approve":
		approvalType = types.ApprovalTypes.Approved
	case "decline":
		approvalType = types.ApprovalTypes.Declined
	}

	approval := &types.Approval{
		Approver:   approver,
		Type:       approvalType,
		ReceivedAt: time.Now(),
	}

	requestID := strings.Split(incomingPayload.Actions[0].Value, ":")[1]
	_, err = h.APIClient.RecordApproval(approval, requestID)
	if err != nil {
		return nil, err
	}

	return approval, nil
}

func (h *BlockActionsEventHandler) renderEditApprovalRequestTemplate(accessRequest *types.AccessRequest, approval *types.Approval) (string, error) {
	type templateContext struct {
		Requestor     string
		Approver      string
		AccessRequest *types.AccessRequest
		Approval      *types.Approval
		IsApproved    bool
		Owners        []string
	}

	owners, _ := h.SlackClient.GetUsernames(accessRequest.Application.Owners)
	requestor, _ := h.SlackClient.GetUsername(accessRequest.Requestor)
	approver, _ := h.SlackClient.GetUsername(approval.Approver)
	context := templateContext{
		Requestor:     requestor,
		Approver:      approver,
		AccessRequest: accessRequest,
		Approval:      approval,
		IsApproved:    approval.Type == types.ApprovalTypes.Approved,
		Owners:        owners,
	}

	return h.Renderer.RenderTemplate("editApprovalRequest", context)
}

func (h *BlockActionsEventHandler) renderEditAccessChangeRequestTemplate(accessRequest *types.AccessRequest, accessChange *types.AccessChange) (string, error) {
	type templateContext struct {
		Requestor     string
		Owner         string
		AccessRequest *types.AccessRequest
		AccessChange  *types.AccessChange
		IsGranted     bool
	}

	requestor, _ := h.SlackClient.GetUsername(accessRequest.Requestor)
	owner, _ := h.SlackClient.GetUsername(accessChange.Owner)
	context := templateContext{
		Requestor:     requestor,
		Owner:         owner,
		AccessRequest: accessRequest,
		AccessChange:  accessChange,
		IsGranted:     (accessChange.Type == types.AccessChangeTypes.Granted),
	}

	return h.Renderer.RenderTemplate("editAccessChangeRequest", context)
}
