package utils

import (
	"os"

	log "github.com/sirupsen/logrus"
)

// ConfigureLogging sets the logging level and format
func ConfigureLogging() {

	configLevel := os.Getenv("LOG_LEVEL")
	if configLevel == "" {
		log.SetLevel(log.InfoLevel)
	} else {
		logLevel, err := log.ParseLevel(configLevel)
		if err != nil {
			log.SetLevel(log.InfoLevel)
		} else {
			log.SetLevel(logLevel)
		}
	}

	log.SetFormatter(&log.TextFormatter{
		DisableColors: true,
		FullTimestamp: true,
	})
}
