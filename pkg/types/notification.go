package types

const (
	requestCreated        = "ACCESS_REQUEST_CREATED"
	approvalReceived      = "APPROVAL_RECEIVED"
	accessChangeRequested = "ACCESS_CHANGE_REQUESTED"
	accessChanged         = "ACCESS_CHANGED"
)

type notificationType struct {
	RequestCreated        string
	ApprovalReceived      string
	AccessChangeRequested string
	AccessChanged         string
}

// NotificationTypes describes the supported webhook notification types
var NotificationTypes = notificationType{
	RequestCreated:        requestCreated,
	ApprovalReceived:      approvalReceived,
	AccessChangeRequested: accessChangeRequested,
	AccessChanged:         accessChanged,
}

// Notification is the payload sent to webhook recipients
type Notification struct {
	Type          string         `json:"notificationType"`
	AccessRequest *AccessRequest `json:"payload"`
	Approval      *Approval      `json:"approval,omitempty"`
	AccessChange  *AccessChange  `json:"accessChange,omitempty"`
}

// Notifier describes a type that can send notifications of relevant events to targets
type Notifier interface {
	Notify(notification *Notification) error
}
