package types

import (
	"time"
)

// AccessRequest records when a user requests access to an application
type AccessRequest struct {
	ID                string          `json:"id" bson:"id"`
	Application       *Application    `json:"application" bson:"application,omitempty"`
	Requestor         string          `json:"requestor" bson:"requestor"`
	RequestedAt       time.Time       `json:"requestedAt" bson:"requestedAt"`
	AccessAgreementAt *time.Time      `json:"accessAgreementAt,omitempty" bson:"accessAgreementAt,omitempty"`
	Approvals         []*Approval     `json:"approvals,omitempty" bson:"approvals"`
	AccessChanges     []*AccessChange `json:"accessChanges,omitempty" bson:"accessChanges"`
}

// IsValid indicates whether or not the AccessRequest properties are complete and valid
func (r *AccessRequest) IsValid() bool {
	isValid := (r.Application.ID != "" && r.Requestor != "" && !r.RequestedAt.IsZero())
	return isValid
}

// IsApprovalRequired indicates if a further approval is required before access should be granted
func (r *AccessRequest) IsApprovalRequired() bool {
	if len(r.Application.Approvers) == 0 {
		return false
	} else if len(r.Approvals) == 0 {
		return true
	} else if r.Approvals[len(r.Approvals)-1].Type == ApprovalTypes.Approved {
		return false
	} else {
		return true
	}
}
