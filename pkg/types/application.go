package types

// Application defines the system users could access, people who can approve access, and those who provision it
type Application struct {
	ID                      string   `json:"id" bson:"id"`
	Name                    string   `json:"name" bson:"name"`
	Description             string   `json:"description" bson:"description"`
	RequiresAccessAgreement bool     `json:"requiresAccessAgreement" bson:"requiresAccessAgreement"`
	Approvers               []string `json:"approvers" bson:"approvers"`
	Owners                  []string `json:"owners" bson:"owners"`
}

// IsValid indicates whether the Application properties are complete and valid
func (a *Application) IsValid() bool {
	isValid := a.Name != "" && len(a.Owners) > 0
	return isValid
}
