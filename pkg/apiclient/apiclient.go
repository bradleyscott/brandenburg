package apiclient

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"gitlab.com/bradleyscott/brandenburg/pkg/types"
)

// APIClient can talk to brandenburg's REST API
type APIClient struct {
	Host string
}

// NewAPIClient initialises an APIClient with the hostname of the brandenburg API
func NewAPIClient(host string) APIClient {
	return APIClient{Host: host}
}

func (c APIClient) get(path string, target interface{}) error {
	url := c.Host + path
	resp, err := http.Get(url)

	if err != nil {
		return err
	}

	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return errors.New("Received HTTP status code " + strconv.Itoa(resp.StatusCode))
	}

	json.NewDecoder(resp.Body).Decode(&target)
	return nil
}

func (c APIClient) post(path string, payload interface{}, target interface{}) error {
	url := c.Host + path
	body, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	var resp *http.Response
	resp, err = http.Post(url, "application/json", bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	if resp.StatusCode >= 300 {
		return errors.New("Received HTTP status code " + strconv.Itoa(resp.StatusCode))
	}

	json.NewDecoder(resp.Body).Decode(&target)
	return nil
}

// GetApplications sends GET /applications
func (c APIClient) GetApplications() ([]*types.Application, error) {
	applications := make([]*types.Application, 0)
	err := c.get("/applications", &applications)
	if err != nil {
		return applications, err
	}
	return applications, nil
}

// GetApplication sends GET /applications/{id}
func (c APIClient) GetApplication(id string) (*types.Application, error) {
	var application *types.Application
	path := "/applications/" + id
	err := c.get(path, &application)
	if err != nil {
		return nil, err
	}
	return application, nil
}

// CreateApplication sends POST /applications with proposed Application in body
func (c APIClient) CreateApplication(proposed *types.Application) (*types.Application, error) {
	err := c.post("/applications", proposed, &proposed)
	if err != nil {
		return nil, err
	}
	return proposed, nil
}

// GetAccessRequest sends GET /accessRequest/{id}
func (c APIClient) GetAccessRequest(id string) (*types.AccessRequest, error) {
	var accessRequest types.AccessRequest
	path := "/accessRequests/" + id
	err := c.get(path, &accessRequest)
	if err != nil {
		return nil, err
	}
	return &accessRequest, nil
}

// CreateAccessRequest sends POST /accessRequests with proposed AccessRequest in body
func (c APIClient) CreateAccessRequest(proposed *types.AccessRequest) (*types.AccessRequest, error) {
	err := c.post("/accessRequests", proposed, &proposed)
	if err != nil {
		return nil, err
	}
	return proposed, nil
}

// RecordAccessChange sends POST /accessRequest/{id}/accessChange with provided AccessChange in body
func (c APIClient) RecordAccessChange(change *types.AccessChange, accessRequestID string) (*types.AccessRequest, error) {
	var accessRequest *types.AccessRequest
	url := "/accessRequests/" + accessRequestID + "/accessChange"
	err := c.post(url, change, &accessRequest)
	if err != nil {
		return nil, err
	}
	return accessRequest, nil
}

// RecordApproval sends POST /accessRequest/{id}/approval with provided Approval in body
func (c APIClient) RecordApproval(approval *types.Approval, accessRequestID string) (*types.AccessRequest, error) {
	var accessRequest *types.AccessRequest
	url := "/accessRequests/" + accessRequestID + "/approval"
	err := c.post(url, approval, &accessRequest)
	if err != nil {
		return nil, err
	}
	return accessRequest, nil
}
