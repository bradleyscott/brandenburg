package main

import (
	"fmt"
	"net/http"
	"os"
	"strings"

	figure "github.com/common-nighthawk/go-figure"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bradleyscott/brandenburg/internal/api"
	"gitlab.com/bradleyscott/brandenburg/internal/utils"
)

func main() {
	figure.NewFigure("brandenburg", "gothic", true).Print()

	fmt.Println(":: brandenburg API starting.... ::")
	godotenv.Load(".env") // Load .env file
	utils.ConfigureLogging()

	// Set up DB
	dbConfig := &api.MongoDBClientConfig{
		Host:     os.Getenv("DB_HOST"),
		Port:     os.Getenv("DB_PORT"),
		User:     os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASSWORD"),
		Database: os.Getenv("DB_NAME"),
	}
	dbClient, err := api.NewMongoDBClient(dbConfig)
	if err != nil {
		log.Panic("Unable to connect to DB")
	}

	// Set up webhooks
	targets := strings.Split(os.Getenv("NOTIFY_TARGETS"), ",")
	notifier, err := api.NewHTTPNotifier(targets)
	if err != nil {
		log.Panic("Unable to configure webhook notifier")
	}

	// Define API routes
	httpHandler := api.NewHTTPHandler(dbClient, notifier)
	r := mux.NewRouter()
	r.HandleFunc("/ping", httpHandler.GetPing).Methods("GET")
	r.HandleFunc("/applications", httpHandler.GetApplications).Methods("GET")
	r.HandleFunc("/applications", httpHandler.PostApplication).Methods("POST")
	r.HandleFunc("/applications/{id}", httpHandler.GetApplication).Methods("GET")
	r.HandleFunc("/accessRequests/{id}", httpHandler.GetAccessRequest).Methods("GET")
	r.HandleFunc("/accessRequests", httpHandler.PostAccessRequest).Methods("POST")
	r.HandleFunc("/accessRequests/{accessRequestID}/approval", httpHandler.PostApproval).Methods("POST")
	r.HandleFunc("/accessRequests/{accessRequestID}/accessChange", httpHandler.PostAccessChange).Methods("POST")

	// Start API
	port := os.Getenv("API_PORT")
	if port == "" {
		port = "3000" // Default to port 3000
	}

	fmt.Printf(":: Listening on port %s :: \n", port)
	h := handlers.LoggingHandler(os.Stdout, r)
	err = http.ListenAndServe(":"+port, h)
	if err != nil {
		log.WithField("err", err.Error()).Panic("Unable to start API")
	}
}
