package main

import (
	"fmt"
	"net/http"
	"os"

	figure "github.com/common-nighthawk/go-figure"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bradleyscott/brandenburg/internal/slack/eventhandlers"
	"gitlab.com/bradleyscott/brandenburg/internal/slack/notificationhandlers"
	"gitlab.com/bradleyscott/brandenburg/internal/slack/routers"
	"gitlab.com/bradleyscott/brandenburg/internal/slack/shared"
	"gitlab.com/bradleyscott/brandenburg/internal/utils"
	"gitlab.com/bradleyscott/brandenburg/pkg/apiclient"
)

func main() {
	figure.NewFigure("brandenburg", "gothic", true).Print()
	fmt.Println(":: brandenburg Slack starting.... ::")
	godotenv.Load(".env") // Load .env file
	utils.ConfigureLogging()

	// Define API routes
	eventRouter, notificationRouter := createRouters()
	r := mux.NewRouter()
	r.HandleFunc("/event", eventRouter.Process).Methods("POST")
	r.HandleFunc("/notification", notificationRouter.Process).Methods("POST")

	// Start API
	port := os.Getenv("SLACK_PORT")
	if port == "" {
		port = "3100" // Default to port 3100
	}

	fmt.Printf(":: Listening on port %s :: \n", port)
	h := handlers.LoggingHandler(os.Stdout, r)
	err := http.ListenAndServe(":"+port, h)
	if err != nil {
		log.WithField("err", err.Error()).Panic("Unable to start Slack")
	}
}

func createRouters() (routers.EventRouter, routers.NotificationRouter) {

	apiURL := os.Getenv("API_URL")
	if apiURL == "" {
		apiURL = "http://api:3000"
	}

	token := os.Getenv("SLACK_TOKEN")
	if token == "" {
		log.Panic("No Slack token provided")
	}

	options := shared.HandlerOptions{
		Renderer:    shared.MustacheRenderer{},
		APIClient:   apiclient.NewAPIClient(apiURL),
		SlackClient: shared.NewClient(token),
	}

	eventRouter := routers.EventRouter{
		ShortcutHandler:       eventhandlers.NewShortcutEventHandler(options),
		ViewSubmissionHandler: eventhandlers.NewViewSubmissionEventHandler(options),
		BlockActionsHandler:   eventhandlers.NewBlockActionsEventHandler(options),
	}

	notificationRouter := routers.NotificationRouter{
		RequestCreatedHandler:   notificationhandlers.NewRequestCreatedNotificationHandler(options),
		ApprovalReceivedHandler: notificationhandlers.NewApprovalReceivedNotificationHandler(options),
		AccessChangedHandler:    notificationhandlers.NewAccessChangedNotificationHandler(options),
	}

	return eventRouter, notificationRouter
}
