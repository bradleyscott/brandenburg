# brandenburg Slack
This executable starts a service that listens for:
* [Slack interaction payloads](https://api.slack.com/interactivity/handling#payloads) on `POST /event`
* brandenburg api notification payloads on `POST /notification`

and interacts with Slack and the brandenburg api accordingly

## Environment variables
| Variable key | Description | Default
|---|---|---|
| LOG_LEVEL | Determines the verbosity of logging | Info
| SLACK_PORT | The port the slack service listens on | 3100
| API_URL | The url of the brandenburg api service | http://api:3000
| SLACK_TOKEN | The Bot User OAuth Access Token of your Slack app | -

