module gitlab.com/bradleyscott/brandenburg

go 1.14

require (
	github.com/cbroglie/mustache v1.0.1
	github.com/common-nighthawk/go-figure v0.0.0-20190529165535-67e0ed34491a
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/google/uuid v1.1.1
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/joho/godotenv v1.3.0
	github.com/sirupsen/logrus v1.6.0
	github.com/slack-go/slack v0.6.4
	github.com/urfave/cli/v2 v2.2.0
	go.mongodb.org/mongo-driver v1.3.3
)
