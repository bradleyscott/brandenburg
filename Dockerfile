FROM golang

RUN mkdir /app
ADD . /app
WORKDIR /app

RUN go mod download
RUN cd /app/cmd/api/ && go build .
RUN cd /app/cmd/slack/ && go build . 
RUN cd /app/cmd/loader/ && go build .